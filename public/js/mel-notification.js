/**
 * Created by Joel on 24/01/2020.
 */




function pushNotifications(msgArray,mode,type) {
    if(type===Notification.type.ERROR){
        msgClass='alert-danger';

    }else if(type===Notification.type.SUCCESS){
        msgClass='alert-success';

    }else if(type===Notification.type.WARNING){
        msgClass='alert-warning';

    }else {
        msgClass='alert-info';

    }

    if(mode===Notification.mode.FORM_NOTIF && formNotifContainer!=null){
            container=$('#'+formNotifContainer);
            container.html('');
            var i=0;
            content='<div class=" '+msgClass+'" ><ul>';
            for(i=0;i<msgArray.length;i++){
                content+='\n<li>'+msgArray[i]+'</li>';
            }
            content+='</ul></div>';

            if(i>0){
                container.html(content);
            }

    }else if(mode===Notification.mode.TOAST_NOTIF){
       showAlert(msgArray,type);
    }

}



function showAlert(text,type){
    var el;
    if(type===Notification.type.ERROR){
        el=$('#js-danger');
    }else
    {
        el=$('#js-alert');
    }
    var textEl=el.find('.alert-text');
    textEl.html(text);
    el.css('left','20px');
    el.css('top','20px');
   // el.css('opacity',1);

    /*setTimeout(function () {
        el.css('opacity',0);

    },3000);*/
    setTimeout(function () {
        el.css('left','-500px');
        //el.css('top','-500px');
    },4500);


}