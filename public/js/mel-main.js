/**
 * Created by Joel on 23/01/2020.
 */

const HTTP  ={
    method:{
        GET:'GET',
        POST:'POST'
    }


};


const Notification  ={
    mode:{
        FORM_NOTIF:0,
        TOAST_NOTIF:1,
        BOX_NOTIF:2
    },

    type:{
        SUCCESS:0,
        ERROR:1,
        WARNING:2,
        INFO:3
    }
};
const DEFAULT_FORM_NOTIF_CONTAINER='form-notification-container';
const DEFAULT_NOTIFICATION_MODE=Notification.mode.TOAST_NOTIF;
const DEFAULT_VIEW_CONTAINER='view-container';
var formNotifContainer=null;
var view_container;

function sanitize(str) {
    str=str.replace(/\'/g,'\\\'');
    str=str.replace(/\n/g,'\\n');
    str=str.replace(/\r/g,'\\r');

    return str;
}