/**
 * Created by Joel on 23/01/2020.
 */
function validateInput(idList) {

    res=true;
    idArray=idList.split(',');
    i=0;
    for(i=0;i<idArray.length;i++){
        if(!isValid(idArray[i]))
            res=false;
    }
    return res;
}

function isValid(elementId){

    valid=true;
    element=$('#'+elementId);
    elValidation=$('#'+elementId+'-validation');
    element.removeClass('input-invalid');
    elValidation.removeClass('input-error');
    elValidation.html('');


    value=element.val();
    rulesArray=element.attr('validate').split('|');
    var rule;


    for(rule in rulesArray){
        if(rulesArray[rule]==='nullable'  && value.length<=0){
            valid=true;
            element.removeClass('input-invalid');

            elValidation.removeClass('input-error');
            elValidation.html('');
            break;
        }
        msg=rulesHandler(rulesArray[rule],value);
        if(msg!=null){
            elValidation.html(msg);
            element.addClass('input-invalid');
            elValidation.addClass('input-error');
            valid=false;
        }
    }

    return valid;
}

function rulesHandler(rule,value) {

    msg=null;
    if(rule==='required'){
        msg=checkRequired(rule,value);

    }else if(rule.startsWith('min:')){

        msg=checkMin(rule,value);

    }else if(rule==='email'){
       msg=checkEmail(rule,value);
    }
    return msg;
}




/******* Rules definitions ********/


function checkRequired(rule,value){
    msg=null;
    if(rule==='required') {
        if (value == null || value.replace(/\s+/g, '').length <= 0) {
            msg = "This field is required";
        }
    }
    return msg;
}

function checkMin(rule,value){
    msg=null;
    if(rule.startsWith('min:')){


        if(rule.length>4){

            size=rule.substring(4);
            size=parseInt(size);

            if(Number.isInteger(size)){
                if(value.length<size){
                    msg="This field must be at least "+size+" characters";

                }
            }
        }

    }
    return msg;
}


function checkEmail(rule,value){
    msg=null;
    if(rule==='email'){
        if(!(/^\w+@\w+\.\w+$/.test(value)))
            msg="Email must be like abc@mail.com";
    }
    return msg;
}

