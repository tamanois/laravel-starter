/**
 * Created by Joel on 23/01/2020.
 */



function sendData2(data,url,method,buttonId,screenLoader,successHandler,errorHandler,notificationContainerId){
    formNotifContainer=notificationContainerId;
    if(formNotifContainer!=null) {
        container = $('#' + formNotifContainer);
        container.html('');
    }
    ajaxCall(data,null,url,method,buttonId,screenLoader,successHandler,errorHandler,null)

}

function sendDataWithFile(data,url,buttonId,screenLoader,successHandler,errorHandler,progressHandler){
    formNotifContainer=DEFAULT_FORM_NOTIF_CONTAINER;
    if(formNotifContainer!=null) {
        container = $('#' + formNotifContainer);
        container.html('');
    }
    ajaxCall(data,null,url,'POST',buttonId,screenLoader,successHandler,errorHandler,progressHandler)

}

function sendData(data,url,method,buttonId,screenLoader,successHandler,errorHandler){
    formNotifContainer=DEFAULT_FORM_NOTIF_CONTAINER;
    if(formNotifContainer!=null) {
        container = $('#' + formNotifContainer);
        container.html('');
    }
    ajaxCall(data,null,url,method,buttonId,screenLoader,successHandler,errorHandler,null)

}

function getView(url,data,method) {
    view_container=DEFAULT_VIEW_CONTAINER;
    ajaxCall(data,null,url,method,null,null,applyView,null,null)

}
function getViewWithLoader(url,data,method,button,loader) {
    view_container=DEFAULT_VIEW_CONTAINER;
    ajaxCall(data,null,url,method,button,loader,applyView,null,null)

}

function getViewCustomContainer(url,data,method,button,loader,viewContainer) {
    view_container=viewContainer;
    ajaxCall(data,null,url,method,button,loader,applyView,null,null)

}

function ajaxCall(data,dataType,url,method,buttonId,screenLoader,successHandler,errorHandler,progressHandler) {
    var dType={};
    var cType=false;
    var mXhr={};
    if(dataType==='json'){
        dType={dataType : 'json'};
        cType='application/json; charset=utf-8';

    }
    var pData=false;
    if(method===HTTP.method.GET){
        pData=true;
    }

    if(progressHandler!=null){
        mXhr={
            xhr: function () {

                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        // For handling the progress of the upload
                        myXhr.upload.addEventListener('progress', function (e) {
                            if (e.lengthComputable) {
                                progressHandler(e.loaded,e.total);
                            }
                        }, false);
                    }
                    return myXhr;

            }
        }
    }

    var parameters={
        url:url,
        type:method,
        data:data,
        processData:pData,
        contentType:cType,
        beforeSend:function () {
            //Gestion du bouton déclencheur s'il existe
            if(buttonId!=null)
                setLoadingState(buttonId);
            //Gestion du loader général
            if(screenLoader)
                startLoading();
        },

        success:function (data) {
            if(successHandler!=null){
                successHandler(data);
            }
            else {
                console.log('success',data);

            }
        },
        error:function (xhr) {

                handleError(xhr,errorHandler);

        },
        complete:function () {
            if(buttonId!=null)
                cancelLoadingState(buttonId);
            if(screenLoader)
                stopLoading();
        }
    };

    var ajaxParameters=Object.assign({},dType,parameters,mXhr);

    $.ajax(ajaxParameters);
}


function handleError(detail,errorHandler){
    console.log('error', detail);

    data=detail.responseJSON;
    dataText=detail.responseText;

    if(detail.status===301 || detail.status===302 || detail.status===307 || detail.status===308){
        if(data){
            console.log('redirected to: '+data.redirectUrl);
        }else{
            console.log('redirected to: '+dataText);
        }
    }else if(detail.status==400){
        if(data){
            errors=data.errors;
        }else{
            errors=dataText;
        }
        if(errorHandler){
            errorHandler(errors);

        }else{
            pushNotifications(errors,DEFAULT_NOTIFICATION_MODE,Notification.type.ERROR);

        }
    }else if(detail.status==401){

    }else if(detail.status==403){

    }
    else if(detail.status==405){
        pushNotifications("Bad request method",DEFAULT_NOTIFICATION_MODE,Notification.type.ERROR);

    }
    else{
        console.log('server error');
        pushNotifications("server error",DEFAULT_NOTIFICATION_MODE,Notification.type.ERROR);
        //$('html').html(detail.responseText);


    }

}

function setLoadingState(buttonId){
    jElement=$('#'+buttonId);
    jElement.addClass('loading-button');
    jElement.addClass('disabled');
    setLocked(buttonId);
}

function cancelLoadingState(buttonId){
    jElement=$('#'+buttonId);
    jElement.removeClass('loading-button');
    jElement.removeClass('disabled');
    setUnlocked(buttonId);
}

function setUnlocked(id) {
    jElement=$('#'+id);
    jElement.removeAttr('locked');

}
function setLocked(id) {
    jElement=$('#'+id);
    jElement.attr('locked','true');
}

function isLocked(id) {
    jElement=$('#'+id);
    return (jElement.attr('locked')==='true');

}

function startLoading() {
    $('#loading-back').css('display','inline');
    $('#loading_icon').css('display','inline');
}
function stopLoading() {
    $('#loading-back').css('display','none');
    $('#loading_icon').css('display','none');
}

function applyView(data) {
    jVC=$('#'+view_container);
    jVC.html(data);
}



