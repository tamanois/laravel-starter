<html>
<head>
    <title>Test</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/mel.css')}}">

    <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('js/mel-main.js')}}"></script>
    <script src="{{asset('js/mel-notification.js')}}"></script>
    <script src="{{asset('js/mel-validation.js')}}"></script>
    <script src="{{asset('js/mel-request.js')}}"></script>


</head>
<body>
    <div class="big-container container">
        <div id="overall-notifications"></div>
        <div id="view-container">
            <h2>My form</h2>
            <div id="form-notification-container">

            </div>

            <div class="row">
                <div class="col-md-4 offset-md-4 pt-5">
                    <form action="" class="form" id="myForm">
                        <div class="form-group">
                            <label for="">file:</label>
                            <input type="file" accept="image/jepg,image/png,video/avi,video/mp4" class="form-control" id="testfile" name="testfile" validate="required">
                            <small id="testfile-validation" class="element-validation"></small>
                        </div>
                        <div class="form-group">
                            <label for="username">username</label>
                            <input type="text" class="form-control" placeholder="username" name="username" id="username" validate="required">
                            <small id="username-validation" class="element-validation"></small>
                        </div>
                        <div class="form-group">
                            <label for="password">password</label>
                            <input type="password" class="form-control" placeholder="password" name="password" id="password" validate="min:6|required">
                            <small id="password-validation" class="element-validation"></small>

                        </div>
                        <button class="btn btn-dark" type="submit" id="login_form_button">
                            <i class="fas fa-user"></i>
                            Se connecter
                        </button>
                    </form>
                </div>
            </div>
        </div>


    </div>
</body>


<script>
    baseUrl='http://127.0.0.1:8080/Projects/Laravel%205.4%20starter/public';
    login_attempt_url=baseUrl+'/login/attempt';
    welcome_url=baseUrl+'/welcome';
</script>
<script>


    $('#myForm').on('submit',function (e) {
        e.preventDefault();
        var jForm= $('#myForm');
        var form=jForm[0];

        fData= new FormData(form);
        buttonID='login_form_button';
            if(!isLocked(buttonID)) {
            if (validateInput('testfile,username,password'))
                sendData(fData,login_attempt_url, HTTP.method.POST, buttonID, false, logged, null);
        }
        });

        function logged(){
            getView(welcome_url,null,HTTP.method.GET);
        }




</script>

<div id="loading_icon">
    <img src="images/loading.gif">
</div>

<div id="loading-back"> </div>

<div id="js-alert" class="alertify bg-primary">
    <div class="alert-content" >
        <strong class="alert-text"></strong>

    </div>

</div>
<div id="js-danger" class="alertify bg-danger">
    <div class="alert-content">
        <strong class="alert-text"></strong>

    </div>

</div>
</html>