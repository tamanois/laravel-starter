@component('mail::message')
Bonjour,

Veuillez télécharger s'il vous plait le rapport journalier ci-joint.


Remerciements,<br>
{{ config('app.name') }}
@endcomponent
