<?php
/**
 * Created by PhpStorm.
 * User: Joel
 * Date: 27/07/2018
 * Time: 07:08
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'devise'=>'FCFA',
    "prix_min"=>"Prix de vente minimal",
    "boutique"=>"Boutique",
    "magasin"=>"Magasin",
    "nouveau"=>"Nouveau(elle)",
    "nom"=>"Nom",
    "active"=>"Activé",
    "desactive"=>"Désactivé",
    "tous"=>"Tous",
    "m_verse"=>"Somme perçue",
    "reste"=>"Relicat",
    "reste_ap"=>"Reste à payer",
    "montant"=>"Montant",
    "mod"=>"Modifier",
    "op"=>"Opération",
    "add"=>"Ajouter",
    "minus"=>"Soustraire",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",
    ""=>"",

];