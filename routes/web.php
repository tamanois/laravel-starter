<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('home');




    Route::get('/unauthorized',function(\Illuminate\Http\Request $request){

        return view('not_auth');
    })->name('not_auth');






Route::group(['middleware'=>'guest'],function () {


    Route::post('/login/attempt','Auth\AuthController@attempt_login')->name('attempt_login');
    Route::post('/register/store','Auth\AuthController@store')->name('store_user');
    Route::get('/register',function(){
        if(Auth::check())
            return redirect(route('home'));
        return view('auth.register');
    })->name('register');
    Route::get('/login',function(){
        if(Auth::check())
            return redirect(route('home'));
        return view('auth.login');
    })->name('login');



});



            Route::group(['middleware'=>'auth2'],function () {



                Route::get('/logout',function(){
                    \Illuminate\Support\Facades\Auth::logout();
                    \Illuminate\Support\Facades\Session::flush();
                    return redirect()->route('login');
                })->name('logout');


                Route::get('/welcome','defaultController@welcome')->name('welcome');

            });




