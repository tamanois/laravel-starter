<?php

namespace L54S;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function groups()
    {
        return  $this->belongsToMany(Group::class,GroupRole::class,'id_role','id_group');
    }
}
