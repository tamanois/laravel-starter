<?php

namespace L54S;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function users()
    {
        return  $this->belongsToMany(User::class,UserGroup::class,'id_group','id_user');
    }

    public function roles()
    {
        return  $this->belongsToMany(Role::class,GroupRole::class,'id_group','id_role');
    }
}
