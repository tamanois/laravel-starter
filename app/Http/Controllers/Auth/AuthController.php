<?php

namespace L54S\Http\Controllers\Auth;

use Auth;
use Illuminate\Support\Facades\Validator;
use L54S\User;
use Illuminate\Http\Request;
use L54S\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct(Request $request)
    {
        if(Auth::check())
        {
            $url=route('home');

            if ($request->ajax()){
                return response()->json(['url'=>$url],302);
            }else{
                return redirect($url);
            }

        }
    }


    public function attempt_login(Request $request)
    {

        if ($request->ajax()){
            $validator=Validator::make($request->all(),[
                'username'=>'required',
                'password'=>'required'
            ]);
            if ($validator->passes()){
                $username=$request->input('username');
                $password=$request->input('password');
                $remember=$request->input('remember');
                if($remember)
                    $remember=true;
                else
                    $remember=false;

                if(Auth::attempt(['username'=>$username,'password'=>$password],$remember)){
                    $u=Auth::user();
                    if($u->active==0){
                        Auth::logout();
                        return response()->json(['errors'=>['Votre compte est désactivé']],302);

                    }

                    return response()->json(['response'=>'OK']);


                }
                else
                    return response()->json(['errors'=>['Le nom d\'utilisateur ou le mot de passe sont incorrects']],400);



            }else{
                return response()->json(['errors'=>$validator->errors()->all()],400);

            }

        }else{
            $this->validate($request,[
                'username'=>'required',
                'password'=>'required'
            ]);
            $username=$request->input('username');
            $password=$request->input('password');
            $remember=$request->input('remember');
            if($remember)
                $remember=true;
            else
                $remember=false;

            if(Auth::attempt(['username'=>$username,'password'=>$password],$remember)){
                $u=Auth::user();
                if($u->active==0){
                    Auth::logout();
                    return redirect()->to(route('login'))->withErrors(['inactive'=>'Votre compte est désactivé']);

                }

                return redirect(route('home'));

            }
            else
                return redirect()->to(route('login'))->withErrors(['bad_auth'=>'Le nom d\'utilisateur ou le mot de passe sont incorrects'])->withInput($request->all());


        }


    }

    public function store(Request $request)
    {
        $url=route('home');


        if ($request->ajax()){
            $validator=Validator::make($request->all(),[
                'username'=>'required|unique:users',
                'email'=>'email|unique:users',
                'name'=>'required',
                'password'=>'required|confirmed|min:6'
            ]);
            if ($validator->passes()){

                $u=new User();
                $u->name=$request->input('name');
                $u->username=$request->input('username');
                $u->password=bcrypt($request->input('password'));
                if($request->input('email'))
                    $u->email=$request->input('email');
                if($request->input('firstname'))
                    $u->firstname=$request->input('firstname');

                $u->save();


                if(Auth::attempt(['username'=>$request->input('username'),'password'=>$request->input('password')])){
                    $u=Auth::user();
                    if($u->active==0){
                        Auth::logout();
                        return response()->json(['response'=>'Votre compte est désactivé'],201);

                    }

                    return response()->json(['response'=>'OK','redirectUrl'=>$url],201);


                }

                return response()->json(['errors'=>["Account created. Unable to login"]],400);




            }else{
                return response()->json(['errors'=>$validator->errors()->all()],400);

            }

        }else{
            $this->validate($request,[
                'username'=>'required|unique:users',
                'name'=>'required',
                'password'=>'required|confirmed|min:6'
            ]);

            $u=new User();
            $u->name=$request->input('name');
            $u->username=$request->input('username');
            $u->password=bcrypt($request->input('password'));
            if($request->input('email'))
                $u->email=$request->input('email');
            if($request->input('firstname'))
                $u->firstname=$request->input('firstname');

            $u->save();
            if(Auth::attempt(['username'=>$request->input('username'),'password'=>$request->input('password')])){
                if($u->active==0){
                    Auth::logout();
                    return redirect()->to(route('login'))->withErrors(['inactive'=>'Votre compte est désactivé'])->withInput($request->all());

                }


                return redirect($url);
            }

        }

    }
}
