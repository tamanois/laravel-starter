<?php

namespace L54S\Http\Middleware;

use Closure;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->ajax()){
            if (\Auth::check())
                return $next($request);
            else
                return response()->json('redirect',401);
        }
        else{
            if (\Auth::check())
                return $next($request);
            else
                return redirect()->route('login');
        }


    }
}
