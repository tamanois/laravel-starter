<?php

namespace L54S\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $url=route('home');
        if (Auth::guard($guard)->check()) {
            if($request->ajax())
                return response()->json(['redirectUrl'=>$url],301);
            return redirect($url);
        }

        return $next($request);
    }
}
